import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class AppService {
  
  client: URLSearchParams = new URLSearchParams()

  constructor(private http: HttpClient) {
    this.client.append('client_id', 'ed8a4b602c4f657a43ce')
    this.client.append('client_secret', 'e75a8a27d3bbe18b497a8a832eaa965299c2bf7d')
  }


  getUser(user = '') {
    return this.http.get(`https://api.github.com/users/${user}?${this.client}`)
  }

  getRepositoryList(user = '') {
    return this.http.get(`https://api.github.com/users/${user}/repos?${this.client}`)
  }

  getFollowingList(user = '') {
    return this.http.get(`https://api.github.com/users/${user}/following?${this.client}`)
  }

}
