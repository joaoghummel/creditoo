export class User {
  public name?: string;
  public login?: string;
  public avatar_url?: string;
  public created_at?: string;
  public bio?: string;
  public location?: string;
}