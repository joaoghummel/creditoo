import { Component } from '@angular/core'
import { AppService } from './app.service'
import { finalize } from 'rxjs/operators'
import { User } from './models/user';
import { Repository } from './models/repository';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {
  
  userSearch: string = 'gaearon'
  user: User = null
  repositoryList: Repository[] = null
  followingList = null
  userIsLoading: boolean = false
  repositoryListIsLoading: boolean = false
  followingListIsLoading: boolean = false

  constructor(private appService: AppService) { }

  getInformations() {
    this.user = null
    this.repositoryList = null
    this.followingList = null
    this.getUser()
    this.getRepositoryList()
    // this.getFollowingList()
  }

  getUser() {
    this.userIsLoading = true
    this.appService.getUser(this.userSearch)
    .pipe(finalize(()=>this.userIsLoading = false))
    .subscribe(
      success => this.user = success as User
    )
  }

  getRepositoryList() {
    this.repositoryListIsLoading = true
    this.appService.getRepositoryList(this.userSearch)
    .pipe(finalize(()=>this.repositoryListIsLoading = false))
    .subscribe(
      success => this.repositoryList = success as Repository[]
    )
  }
  
  // getFollowingList() {
  //   this.repositoryListIsLoading = true
  //   this.appService.getFollowingList(this.userSearch)
  //   .pipe(finalize(()=>this.followingListIsLoading = false))
  //   .subscribe(
  //     success => this.followingList = success
  //   )
  // }

}