# Creditoo
Para criação do projeto foi utilizado o [Angular CLI](https://github.com/angular/angular-cli) na versão 8.1.0.

## Necessário
Node/npm

## Instalando
1. Faça um git clone deste projeto
1. Utilize o comando **npm install** para instalar as dependências
1. Utilize o comando **ng serve** para rodar o servidor de desenolvimento

## App
Não consegui finalizar o desenvolvimento do jeito que gostaria.

### Faltou
- Criar uma abstração do serviço do github responsável por adicionar o header que eles pedem
- Validar erros nas requisições(Só estou usando sucesso, não faço nada se da erro)
- Componetizar tabs
- Melhorar arquitetura do sass
- Adicionar outras tabs como following and followers

## Tdd
Não deu tempo de tentar desenvolver.